import React, {
    useCallback,
    useEffect,
    useMemo,
    useRef,
    useState
} from 'react';
import Card from '../models/Card';
import SlideComponent from '../models/SlideComponent';
import LoadingIcon from '../models/LoadingIcon';
import Input from '../helpers/Input';
import * as constants from '../constants';
import Dialog from '../models/Dialog';

function Home() {

    useEffect(() => {
        fetchUser();
    }, []);

    const [users, setUsers] = useState(useMemo(() => []));
    const [isLoading, setLoading] = useState(false);
    const [dialogVisible, setDialogVisible] = useState(false);
    const [curView, setCurView] = useState('all');
    const slideComp = useRef();

    const fetchUser = function () {
        setLoading(true);
        fetch(`${constants.default.API_MAIN_URL}/user?limit=4`, {
            headers: new Headers({
                "app-id": constants.default.API_TOKEN
            }),
        })
            .then(response => response.json())
            .then(response => {
                // console.log(response.data);
                // setUsers(response.data);
                setCurView('all');
                setLoading(false);
                // let tempData = [...users];
                // tempData.push(...response.data);
                setUsers(response.data);
            })
            .catch(error => { console.log(error) });
    };

    const userDislike = function () {
        slideComp.current.forward();
    };

    const userLike = function () {
        slideComp.current.forward();
    };

    const getAge = function (id) {
        return fetch(`${constants.default.API_MAIN_URL}/user/${id}`, {
            headers: new Headers({
                "app-id": constants.default.API_TOKEN,
            }),
        })
    };

    const homeContainerElm = useCallback((node) => {
        // console.log(node);
        if (!node)
            return;
        Input(node, function (evt, dir, phase, swipetype, distance) {
            if (phase == 'start') {

            }
            else if (phase == 'move') {

            }
            else if (phase == 'end') {
                if (dir == 'up' && Math.abs(distance) > 150) {
                    setDialogVisible(true);
                }
            }
        });
    }, []);

    const dialogContent = useCallback(() => {
        return (
            <div style={{ height: '100%' }}>
                <button onClick={getLiked} className={"base-button instagram"} style={{ height: '70px', padding: '5%', transform: 'translateY(80%)' }}>
                    <span className={"gradient"}></span>Liked
                </button>
                <button onClick={getDisliked} className={"base-button instagram"} style={{ height: '70px', padding: '5%', transform: 'translateY(120%)' }}>
                    <span className={"gradient"}></span>Disliked
                </button>
            </div>
        );
    }, []);

    const getLiked = function () {
        // console.log('get liked');
        let items = JSON.parse(localStorage.getItem('liked'));
        if (items.length == 0) {
            fetchUser();
        }
        else {
            setUsers(items);
            setCurView('like');
        }
        setDialogVisible(false);
        slideComp.current.reset();
    };

    const getDisliked = function () {
        // console.log('get liked');
        let items = JSON.parse(localStorage.getItem('disliked'));
        if (items.length == 0) {
            fetchUser()
        }
        else {
            setUsers(items);
            setCurView('dislike');
        }

        setDialogVisible(false);
        slideComp.current.reset();
    };

    return (
        (users.length == 0 || isLoading)
            ? (<LoadingIcon className={`absolute-center`}></LoadingIcon>)
            : (
                <div className={'homeContainer'} ref={homeContainerElm}>
                    <SlideComponent
                        ratio={`3:2`}
                        mode={`manual`}
                        ref={slideComp}
                        onViewCompleted={fetchUser}
                        components={
                            users.map((user, index) =>
                                <Card
                                    key={user.id}
                                    cardImg={user.picture}
                                    cardTitle={`${user.firstName} ${user.lastName}`}
                                    userId={user.id}
                                    onDislike={userDislike}
                                    onLike={userLike}
                                    onAge={getAge}
                                    isFirstPerson={index == 0}
                                    view={curView}
                                />
                            )}
                    >
                    </SlideComponent>
                    <Dialog
                        show={dialogVisible}
                        content={dialogContent}
                        onVisibleChange={(visible) => setDialogVisible(visible)}
                    ></Dialog>
                </div>
            )
    );
}

export default Home;