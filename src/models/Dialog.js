import Backdrop from './Backdrop';
import '../css/Dialog.css';
import { useEffect, useState } from 'react';

function Dialog(props) {
    const [visible, setVisible] = useState(false);

    useEffect(() => {
        setVisible(props.show);
    }, [props.show]);

    useEffect(() => {
        props.onVisibleChange && props.onVisibleChange(visible);
    }, [visible]);

    return (
        <div id={'dialogContainer'} className={`${visible ? 'fadeIn' : 'fadeOut'}`} style={{ zIndex: visible ? 1000 : -1 }}>
            <Backdrop onClick={() => setVisible(false)} ></Backdrop>
            <div id={"dialog"} style={{ padding: '15px', backgroundColor: 'white', borderRadius: '40px', width: '50%', height: '30%' }}>
                {/* {props.content} */}
                <props.content></props.content>
            </div>
        </div>
    );
}
export default Dialog;