import '../css/SlideShow.css';
import React, {
    useEffect,
    useImperativeHandle,
    useRef,
    useState,
} from 'react';
import { usePrevious } from '../hooks';
import Input from '../helpers/Input';

function SlideComponent(props, ref) {
    var automaticInterval;
    const containerElm = useRef(null);
    const childElmDom = useRef(null);
    const [slideIndex, setSlide] = useState(0);
    const prevIndex = usePrevious(slideIndex); // will use later

    useEffect(() => {
        // updateDimensions();
        Input(containerElm.current, function (evt, dir, phase, swipetype, distance) {
            if (phase == 'start') {
                let e = new CustomEvent('userStartSwipe', {});
                window.dispatchEvent(e);
            }
            else if (phase == 'move') {
                let e = new CustomEvent('userSwiping', { detail: { distance, dir } });
                window.dispatchEvent(e);
            }
            else if (phase == 'end') {
                let e = new CustomEvent('userEndSwipe', { detail: { distance, dir } });
                window.dispatchEvent(e);
            }
        });
        window.addEventListener("resize", updateDimensions);

        return () => {
            window.removeEventListener("resize", updateDimensions);
            if (automaticInterval) clearTimeout(automaticInterval);
        }
    }, []);

    useEffect(() => {
        if (props.mode === "automatic") {
            const timeout = props.timeout || 5000;

            automaticInterval = setTimeout(
                runAutomatic,
                Number.parseInt(timeout)
            );
        }
        return () => {
            if (automaticInterval) clearTimeout(automaticInterval);
        };
    });

    useImperativeHandle(ref, () => ({
        forward: () => {
            forward();
        },
        reset: () => {
            setSlide(0);
        }
    }));


    const forward = function () {
        setSlide(getNewSlideIndex(1));
    };

    const backward = function () {
        setSlide(getNewSlideIndex(-1));
    };

    const setSlideIndex = function (index) {
        setSlide(index);
    };

    const getNewSlideIndex = function (step) {
        const index = slideIndex;
        const numberSlide = props.components.length;

        let newSlideIndex = index + step;
        if (newSlideIndex == numberSlide) props.onViewCompleted && props.onViewCompleted();
        newSlideIndex = Math.max(Math.min(newSlideIndex, numberSlide - 1), 0);

        return newSlideIndex;
    };

    const updateDimensions = function () {
    };

    const runAutomatic = function () {
        forward();
    };

    return (
        (
            <div className="lp-slideshow" ref={ref}>
                <div className="container" ref={containerElm}>
                    <div id={`slideChildElms`} ref={childElmDom}>
                        {
                            props.components.map((child, index) =>
                                React.cloneElement(child, {
                                    className: `slide ${slideIndex === index ? 'active' : 'inactive'}`,
                                })
                            )
                        }
                    </div>
                </div>
            </div>
        )
    );
}

export default React.forwardRef(SlideComponent);