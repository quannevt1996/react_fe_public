import React, { useCallback, useEffect, useRef, useState } from 'react';
import '../css/Card.css';
import LoadingIcon from './LoadingIcon';

function Card(props) {
    const container = useRef(null);
    const statusOverlay = useRef(null);
    const [likeStatus, setLike] = useState('');
    const [clickStatus, setClick] = useState('');
    const [age, setAge] = useState(0);

    const dislike = function (e) {
        e.stopPropagation();
        container.current.classList.add('transitioning');
        statusOverlay.current.classList.add('scale-to-normal');
        setClick('dislike');
        setTimeout(function () {
            container.current.classList.remove('transitioning');
            container.current.style.transform = null;
            props.onDislike && props.onDislike();
            setLike('dislike-profile');
        }, 1200);
    };

    const like = function (e) {
        e.stopPropagation();
        container.current.classList.add('transitioning');
        statusOverlay.current.classList.add('scale-to-normal');
        setClick('like');
        setTimeout(function () {
            container.current.classList.remove('transitioning');
            container.current.style.transform = null;
            props.onLike && props.onLike();
            setLike('like-profile');
        }, 1200);
    };

    const endSwipe = useCallback(function (data) {
        if (isActive()) {
            container.current.classList.add('card-transition');
            let distance = data.detail.distance;
            let dir = data.detail.dir;
            if (Math.abs(distance) < 200) {
                container.current.style.transform = null;
                return;
            }
            switch (dir) {
                case 'left':
                    // case 'right':
                    props.onDislike && props.onDislike();
                    setLike('dislike-profile');
                    break;

                // case 'up':
                case 'down':
                    props.onLike && props.onLike();
                    setLike('like-profile');
                    break;
            }
        }
    }, []);

    const swiping = useCallback(function (data) {
        if (isActive()) {
            let distance = data.detail.distance;
            let dir = data.detail.dir;
            switch (dir) {
                case 'left':
                    container.current.style.transform = `rotateZ(${distance / 60}deg) translateX(${distance / 1.5}px)`;
                    break;

                case 'down':
                    container.current.style.transform = `rotateZ(${-distance / 60}deg) rotateX(${distance / 20}deg) translateY(${distance / 1.5}px)`;
                    break;
            }
        }
    }, []);

    const startSwipe = useCallback(function () {
        if (isActive()) {
            container.current.classList.remove('card-transition');
        }
    }, []);

    const isActive = useCallback((node) => {
        if (!container.current)
            return false;
        else return container.current.classList.contains('active') && !container.current.classList.contains('transitioning')
    }, []);

    const removeSwipe = function () {
        window.removeEventListener('userEndSwipe', endSwipe);
        window.removeEventListener('userSwiping', swiping);
        window.removeEventListener('userStartSwipe', startSwipe);
    };

    const addSwipe = function () {
        window.addEventListener('userEndSwipe', endSwipe);
        window.addEventListener('userSwiping', swiping);
        window.addEventListener('userStartSwipe', startSwipe);
    }

    useEffect(() => {
        return () => {
            setAge(0);
        }
    }, []);

    useEffect(() => {
        if (age == 0) {
            if (container.current.classList.contains('active') || props.isFirstPerson) {
                console.log('on enter');
                addSwipe();
                props.onAge(props.userId)
                    .then(response => response.json())
                    .then(response => {
                        let year = new Date(response.dateOfBirth).getFullYear();
                        setAge(new Date().getFullYear() - year);
                    })
                    .catch(error => { console.log(error) });
            }
            else {
                removeSwipe();
            }
        }
    });

    useEffect(() => {
        if (!localStorage.getItem('disliked'))
            localStorage.setItem('disliked', '[]');
        if (!localStorage.getItem('liked'))
            localStorage.setItem('liked', '[]');
        let disliked = JSON.parse(localStorage.getItem('disliked'));
        let liked = JSON.parse(localStorage.getItem('liked'));
        let itemInLike = null;
        let itemInDislike = null;
        switch (likeStatus) {
            case 'like-profile':
                itemInLike = liked.find(item => item.id == props.userId);
                itemInDislike = disliked.find(item => item.id == props.userId);
                if (!itemInLike)
                    liked.push({ id: props.userId, picture: props.cardImg, firstName: props.cardTitle, lastName: '', age: age });
                if (itemInDislike)
                    disliked.splice(disliked.indexOf(itemInDislike), 1);
                break;

            case 'dislike-profile':
                itemInLike = liked.find(item => item.id == props.userId);
                itemInDislike = disliked.find(item => item.id == props.userId);
                if (!itemInDislike)
                    disliked.push({ id: props.userId, picture: props.cardImg, firstName: props.cardTitle, lastName: '', age: age });
                if (itemInLike)
                    liked.splice(disliked.indexOf(itemInDislike), 1);
                break;
        }
        localStorage.setItem('liked', JSON.stringify(liked));
        localStorage.setItem('disliked', JSON.stringify(disliked));
    }, [likeStatus]);

    return (
        <div className={(props.className ? `${props.className} card card-transition` : 'card card-transition') + " " + likeStatus} ref={container}>
            <img src={props.cardImg} alt={"Avatar"} style={{ width: "100%" }} />
            <div className="cardContainer">
                <h4 style={{ fontSize: '2em' }}><b>{props.cardTitle}</b></h4>
                {age == 0
                    ? <LoadingIcon style={{ transform: 'scale(0.35)', transformOrigin: 'center top' }}></LoadingIcon>
                    : <h4 style={{ fontSize: '1.5em' }}><b>{age}</b></h4>}
                <p>{props.cardDesc}</p>
            </div>
            <div className={"btn-group"} style={{ display: 'flex', width: '100%', position: 'absolute', bottom: 0 }}>
                <button
                    className={`transparent`}
                    onMouseDown={dislike}
                    onTouchStart={dislike}
                    disabled={clickStatus != ''}
                    style={{ display: props.view == 'all' || props.view == 'like' ? 'block' : 'none' }}
                >
                    <i className={"fas fa-times"}></i>
                </button>
                <button
                    className={`transparent`}
                    onMouseDown={like}
                    onTouchStart={like}
                    disabled={clickStatus != ''}
                    style={{ display: props.view == 'all' || props.view == 'dislike' ? 'block' : 'none' }}
                >
                    <i className={"far fa-heart"}></i>
                </button>
            </div>
            <div className={`card-overlay`} ref={statusOverlay}>
                <i className={"far fa-heart"} style={{ color: 'pink', display: clickStatus != 'like' ? 'none' : 'block' }}></i>
                <i className={"fas fa-times"} style={{ color: 'grey', display: clickStatus != 'dislike' ? 'none' : 'block' }}></i>
            </div>
        </div>
    );
}

export default Card;