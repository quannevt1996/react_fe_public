function Backdrop(props) {
    return (
        <div {...props} style={{ backgroundColor: 'black', opacity: 0.7, width: '100%', height: '100%', top: 0, left: 0 }}>

        </div>
    );
}
export default Backdrop;