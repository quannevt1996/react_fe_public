import Home from './views/Home';
import User from './views/User';
import SlideShow from './views/Home';

const routes = [
    {path: '/home', exact: true, name: 'Home', component: Home},
    {path: '/user/:id', exact: false, name: 'User', component: User},
];

export default routes;