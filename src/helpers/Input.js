function ontouch(el, callback) {

    var touchsurface = el,
        dir,
        swipeType,
        startX,
        startY,
        distX,
        distY,
        dist,
        threshold = 700, //required min distance traveled to be considered swipe
        restraint = 100, // maximum distance allowed at the same time in perpendicular direction
        allowedTime = 400,
        elapsedTime,
        startTime,
        isTouchStart = false,
        handletouch = callback || function (evt, dir, phase, swipetype, distance) { }

    function handleDown(e) {
        if (e.target.tagName == "BUTTON") {
            return;
        }
        isTouchStart = true;
        // var touchobj = e.changedTouches[0];
        var touchobj = e.type.includes('mouse') ? e : e.changedTouches[0];
        dir = 'none';
        swipeType = 'none';
        dist = 0;
        startX = touchobj.pageX;
        startY = touchobj.pageY;
        startTime = new Date().getTime(); // record time when finger first makes contact with surface
        handletouch(e, 'none', 'start', swipeType, 0); // fire callback function with params dir="none", phase="start", swipetype="none" etc
        e.preventDefault();
    }

    function handleMove(e) {
        if (!isTouchStart || e.target.tagName == "BUTTON")
            return;
        // console.log(e.type);
        var touchobj = e.type.includes('mouse') ? e : e.changedTouches[0];
        distX = touchobj.pageX - startX; // get horizontal dist traveled by finger while in contact with surface
        distY = touchobj.pageY - startY; // get vertical dist traveled by finger while in contact with surface
        if (Math.abs(distX) > Math.abs(distY)) { // if distance traveled horizontally is greater than vertically, consider this a horizontal movement
            dir = (distX < 0) ? 'left' : 'right';
            handletouch(e, dir, 'move', swipeType, distX); // fire callback function with params dir="left|right", phase="move", swipetype="none" etc
        }
        else { // else consider this a vertical movement
            dir = (distY < 0) ? 'up' : 'down';
            handletouch(e, dir, 'move', swipeType, distY); // fire callback function with params dir="up|down", phase="move", swipetype="none" etc
        };
        e.preventDefault(); // prevent scrolling when inside DIV
    }

    function handleUp(e) {
        if (e.target.tagName == "BUTTON")
            return;
        isTouchStart = false;
        // var touchobj = e.changedTouches[0];
        var touchobj = e.type.includes('mouse') ? e : e.changedTouches[0];
        elapsedTime = new Date().getTime() - startTime;// get time elapsed
        if (elapsedTime <= allowedTime) { // first condition for awipe met
            if (Math.abs(distX) >= threshold && Math.abs(distY) <= restraint) { // 2nd condition for horizontal swipe met
                swipeType = dir; // set swipeType to either "left" or "right"
            }
            else if (Math.abs(distY) >= threshold && Math.abs(distX) <= restraint) { // 2nd condition for vertical swipe met
                swipeType = dir; // set swipeType to either "top" or "down"
            }
        };
        // Fire callback function with params dir="left|right|up|down", phase="end", swipetype=dir etc:
        handletouch(e, dir, 'end', swipeType, (dir == 'left' || dir == 'right') ? distX : distY);
        resetValue();
        e.preventDefault();
    }

    function resetValue() {

    }

    touchsurface.addEventListener('touchstart', function (e) {
        handleDown(e);
    }, false)

    touchsurface.addEventListener('mousedown', function (e) {
        handleDown(e);
    }, false);

    touchsurface.addEventListener('touchmove', function (e) {
        handleMove(e);
    }, false)

    touchsurface.addEventListener('mousemove', function (e) {
        handleMove(e);
    }, false)

    touchsurface.addEventListener('touchend', function (e) {
        handleUp(e);
    }, false)

    touchsurface.addEventListener('mouseup', function (e) {
        handleUp(e);
    }, false)
}

export default ontouch;